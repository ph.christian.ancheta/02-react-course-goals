# Course Goals - React App
A simple React App that allows users to add course goals, list them, and delete them on click

## Setup

```
npm install
npm run start
```
